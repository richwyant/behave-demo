from behave import *
from utils.driverUtils import *

# TODO: threadlock this when time allows
windowsInPlay = []

@given('open "{browser}" at "{url}"')
def step_impl(context, url, browser):
    global windowsInPlay
    context.browser = newBrowser(url, browser)
    windowsInPlay.append(context.browser.window_handles[0])   

# TODO: explore wild card annotations to avoid duplicate code
@then('open "{browser}" at "{url}"')
def step_impl(context, url, browser):
    global windowsInPlay
    context.browser = newBrowser(url, browser)
    windowsInPlay.append(context.browser.window_handles[0])

@then('open a new window at "{url}"')
def step_impl(context, url):
    global windowsInPlay
    windowsInPlay.append(newWindow(context.browser, url))

@then('switch back to browser "{browserNum}"')
def step_impl(context, browserNum):
    global windowsInPlay
    context.browser.switch_to.window(windowsInPlay[int(browserNum) - 1])
    context.browser.maximize_window()

@then('close the window')
def step_impl(context):
    context.browser.close()

@then('click on the "{text}" field')
def step_impl(context, text):
    click(context.browser, xpathForPlaceholder(text))

@then('click on the "{text}" button')
def step_impl(context, text):
    click(context.browser, "//*[@value='" + text + "']")

@then('click on "{text}"')
def step_impl(context, text):
    click(context.browser, "//*[text()='" + text + "']")

@when('the Login button appears')
def step_impl(context):
    waitForElement(context.browser, "//*[@value='Login']")

@then('the message "{text}" appears')
def step_impl(context, text):
    waitForElement(context.browser, xpathForText(text))

@then('this message appears')
def step_impl(context):
    waitForElement(context.browser, xpathForText(context.text))    

@then('enter "{entryText}" over the "{placeHolderText}" placeholder')
def step_impl(context, entryText, placeHolderText):
    typeOverPlaceholder(context.browser, entryText, placeHolderText)

@then('enter the below over the "{placeHolderText}" placeholder')
def step_impl(context, placeHolderText):
    typeOverPlaceholder(context.browser, context.text, placeHolderText)

@then('click on the "{labelText}" label, tab over and enter')
def step_impl(context, labelText):
    typeInLabeledField(context.browser, context.text, labelText)

@then('verify "{expectedText}" appears in the "{id}" paragraph')
def step_impl(context, expectedText, id):
    verifyTextInP(context.browser, expectedText, id)

@then('do the elements smoke test')
def step_impl(context):
    context.execute_steps('''
    Then click on "Elements"
    Then the message "Please select an item from left to start practice." appears
    Then click on "Text Box"
    Then enter "Richard Wyant" over the "Full Name" placeholder
    Then enter "richard.wyant@protonmail.com" over the "name@example.com" placeholder
    Then enter the below over the "Current Address" placeholder
      """
      secret moon base
      apt #4b
      Rochester, MN 90210
      """
    Then click on "Submit"
    Then the message "Current Address :secret moon base apt #4b Rochester, MN 90210" appears
    ''')

@then('add the permanent address')
def step_impl(context):
    context.execute_steps('''
    Then click on the "Permanent Address" label, tab over and enter
      """
      1273 Rockefeller St
      Cringe, ME 
      """
    Then click on "Submit"
    ''')


    




