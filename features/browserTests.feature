Feature: browser tests for demoqa.com

  Scenario: demoqa.com Elements page
    Given open "Firefox" at "https://demoqa.com/"
    Then do the elements smoke test
    Then open a new window at "https://www.saucedemo.com/"
    Then the message "Swag Labs" appears  
    Then switch back to browser "1"
    Then the message "Current Address :secret moon base apt #4b Rochester, MN 90210" appears
    