from behave import fixture, use_fixture
from selenium import webdriver
from utils.driverUtils import *

def before_feature(context, feature):
    global weAreHeadless
    # TODO: read options from a text file
    # TODO: read options from arguments
    weAreHeadless = False

def after_scenario(context, scenario):
    context.browser.quit()