# from tutorial here: https://behave.readthedocs.io/en/stable/tutorial.html#features
# leaving it in as a smoke test
@smoke
Feature: showing off behave

  Scenario: run a simple test
     Given we have behave installed
      When we implement a test
      Then behave will test it for us!
