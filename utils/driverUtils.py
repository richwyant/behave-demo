from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.proxy import Proxy, ProxyType
from selenium.common.exceptions import InvalidSessionIdException, NoSuchElementException, TimeoutException, ElementClickInterceptedException
import os
from utils.genericUtils import *
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import clipboard

timeout = 5
# TODO: break this up into smaller utility classes

def showWindow(driver:webdriver):
    """
    Sets the window in a certain area

    Arguments:
        driver:WebDriver object
    """
    # driver.set_window_size(1000,600)
    # driver.set_window_position(50,50)
    driver.maximize_window()
    
def newBrowser(url:str, flag:str, headless=False, options=False):
    """
    Creates a new Firefox Webdriver object
    Arguments:
        url: url to open
        flag: "Firefox" or "Chrome"
        headless: toggle to run in headless mode, default=False
        options: optional webdriver.FirefoxOptions object
    Returns: 
        webdriver.Firefox or .Chrome object
    """
    #TODO: Change to match/case once I figure out why upgrading python is failing
    if flag == "Firefox":
        if not options: options = webdriver.FirefoxOptions()
    elif flag == "Chrome":
        if not options: options = webdriver.ChromeOptions()
    else:    
        assert False, flag + " not a supported browser"

    if headless: options.add_argument('--headless')
        
    if flag == "Firefox":
        driver = webdriver.Firefox(options=options)
    else:
        driver = webdriver.Chrome(options=options)

    if not headless: showWindow(driver)   
    driver.get(url)
    return driver

def newWindow(driver:webdriver, url:str):
    """
    Opens a new window with the current browser in context
    Arguments:
        driver:WebDriver object
        url: url to open
    Returns: 
        window handle for new window
    """
    driver.switch_to.new_window('window')
    driver.get(url)
    return driver.window_handles[0]

def waitForElement(driver:webdriver, selector:str, timeout=timeout):
    """
    Returns single element from an xpath selector
    'But why not CSS?'
    https://stackoverflow.com/questions/1520429/is-there-a-css-selector-for-elements-containing-certain-text

    Arguments:
        driver: WebDriver object
        selector: Xpath of element to find
        timeout: optional timeout in seconds
    Returns:
        element
    """
    try:
        return WebDriverWait(driver, timeout).until(
            EC.visibility_of_element_located
            ((By.XPATH, selector)))
    except Exception as e:
        # screenshot(driver, e.__class__.__name__ + "exception")
        raise e 

def waitForElements(driver:webdriver, selector:str, timeout=timeout):
    """
    Returns multiple elements from an xpath selector

    Arguments:
        driver: WebDriver object
        selector: Xpath of elements to find
        timeout: optional timeout in seconds
    Returns:
        element collection
    """
    try:
        return WebDriverWait(driver, timeout).until(
            EC.visibility_of_all_elements_located
            ((By.XPATH, selector)))
    except Exception as e:
        # screenshot(driver, e.__class__.__name__ + "exception")
        raise e 

def screenshot(driver:webdriver, endOfFileName:str):
    """
    Creates a screenshot with a timestamp as part of its filename

    Arguments:
        driver: WebDriver object
        endOfFileName: the rest of the filename after the timestamp
    """
    try:
        driver.get_screenshot_as_file(timestamp() + endOfFileName + ".png")
    except (UnboundLocalError, InvalidSessionIdException) as e:
        print(timestamp() + "unable to capture screenshot")
        print(e.msg)
        pass

def xpathForText(text:str):
    """
    Builds an Xpath selector to find a string anywhere on the page

    Arguments:
        text: text to find

    Returns:    
        Xpath selector to find the text
    """
    return "//*[contains(normalize-space(.),'" + text + "')]"

def xpathForPlaceholder(text:str):
    """
    Builds an Xpath selector to find an
    element with a given placeholder value

    Arguments:
        text: text in the placeholder attribute

    Returns:    
        Xpath selector to find the element
    """
    return "//*[@placeholder='" + text + "']"


def waitForAbsence(driver:webdriver, selector:str, timeout=timeout):
    """
    Waits for a specific element to become invisible.
    Does not fail the test if the element isn't visible in the first place.

    Arguments:
        driver: WebDriver object
        selector: Xpath selector
        timeout: optional timeout in seconds
    """
    try:
        WebDriverWait(driver, 2).until(
            EC.visibility_of_any_elements_located
            ((By.XPATH, selector)))
        WebDriverWait(driver, timeout).until(
            EC.invisibility_of_element
            ((By.XPATH, selector)))
    except (NoSuchElementException, TimeoutException):
        pass

def clickIfPresent(driver:webdriver, selector:str, timeout=timeout):
    """
    Clicks on an element once it's visible.
    Does not fail the test if element is never clicked.

    Arguments:
        driver: WebDriver object
        selector: Xpath selector
        timeout: optional timeout in seconds
    """
    try:
        waitForElement(driver, selector, timeout).click
    except (NoSuchElementException, TimeoutException):
        pass

def click(driver:webdriver, selector:str, timeout=timeout):
    """
    Clicks on an element once it's visible.
    Fails the test if element is never clicked.

    Arguments:
        driver: WebDriver object
        selector: Xpath selector
        timeout: optional timeout in seconds
    Returns:
        WebElement that was clicked for method chaining purposes
    """
    element = waitForElement(driver, selector, timeout)
    try:
        element.click()
    except(ElementClickInterceptedException):
        driver.execute_script("arguments[0].scrollIntoView();", element)
        element.click()

    return element

def typeOverPlaceholder(driver:webdriver, entryText:str, placeHolderText:str):
    """
    Enters text over placeholder text in an input.

    Arguments:
        driver: WebDriver object
        entryText: text to enter
        placeholderText: text value of the placeholder
    """
    click(driver, xpathForPlaceholder(placeHolderText)).send_keys(entryText)

def typeInLabeledField(driver:webdriver, entryText:str, labelText:str):
    """
    Enters text in an element with a corresponding label.

    Arguments:
        driver: WebDriver object
        entryText: text to enter
        labelText: text value of the label
    """
    click(driver, xpathForText(labelText))
    ActionChains(driver)\
        .send_keys(Keys.TAB)\
        .send_keys(entryText)\
        .perform()
    
# def verifyTextInP(driver:webdriver, expectedText:str, id:str):
#     string = "//p[@id='" + id + "' and contains(normalize-space(.),'" + expectedText + "')]"
#     waitForElement(driver, string)
